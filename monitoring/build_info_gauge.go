package monitoring

import "github.com/prometheus/client_golang/prometheus"

// GitlabBuildInfoGaugeMetricName is the name of the label containing build information for this process
const GitlabBuildInfoGaugeMetricName = "gitlab_build_info"
const buildInfoVersionLabel = "version"
const buildInfoBuildTimeLabel = "built"

// registerBuildInfoGauge registers a label with the current server version
// making it easy to see what versions of the application are running across a cluster
func registerBuildInfoGauge(labels prometheus.Labels) {
	gitlabBuildInfoGauge := prometheus.NewGauge(prometheus.GaugeOpts{
		Name:        GitlabBuildInfoGaugeMetricName,
		Help:        "Current build info for this GitLab Service",
		ConstLabels: labels,
	})

	prometheus.MustRegister(gitlabBuildInfoGauge)
	gitlabBuildInfoGauge.Set(1)
}
