package grpccorrelation

// The configuration for InjectCorrelationID
type clientInterceptConfig struct {
	clientName string
}

// ClientCorrelationInterceptorOption will configure a correlation handler
// currently there are no options, but this gives us the option
// to extend the interface in a backwards compatible way
type ClientCorrelationInterceptorOption func(*clientInterceptConfig)

func applyClientCorrelationInterceptorOptions(opts []ClientCorrelationInterceptorOption) clientInterceptConfig {
	config := clientInterceptConfig{}
	for _, v := range opts {
		v(&config)
	}

	return config
}

// WithClientName will configure the client name metadata on the
// GRPC client interceptors
func WithClientName(clientName string) ClientCorrelationInterceptorOption {
	return func(config *clientInterceptConfig) {
		config.clientName = clientName
	}
}
